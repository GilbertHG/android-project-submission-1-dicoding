package com.latihan.submission1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {
    private ArrayList<Country> list = new ArrayList<>();
    public static final String EXTRA_IMG = "extra_photo";
    public static final String EXTRA_COUNTRY = "extra_country";
    public static final String EXTRA_FORMED_YEAR = "extra_formed_year";
    public static final String EXTRA_HISTORY = "extra_history";
    public static final String EXTRA_INDEX = "extra_index";

    ImageView imgCountry;
    TextView tvCountry, tvFormedYear, tvHistory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        imgCountry = findViewById(R.id.img_country_flag);
        tvCountry = findViewById(R.id.country_name);
        tvFormedYear = findViewById(R.id.formed_year);
        tvHistory = findViewById(R.id.history);

        imgCountry.setImageResource(getIntent().getIntExtra(EXTRA_IMG,0));
        tvCountry.setText(getIntent().getStringExtra(EXTRA_COUNTRY));


        int index = getIntent().getIntExtra(EXTRA_INDEX, 0);
        list.addAll(CountryData.getListData());
        Country country = list.get(index);

        tvFormedYear.setText(country.getFormed_year());
        tvHistory.setText(country.getHistory());


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
