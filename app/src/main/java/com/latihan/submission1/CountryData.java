package com.latihan.submission1;

import java.util.ArrayList;

public class CountryData {
    private static String[] countryNames = {
            "Indonesia",
            "Argentina",
            "Belgia",
            "Kanada",
            "Kolombia",
            "Jerman",
            "Jepang",
            "Malaysia",
            "Amerika",
            "Belanda",
    };

    private static String[] formedYear = {
            "1945",
            "1810",
            "1830",
            "1867",
            "1810",
            "1990",
            "660 SM",
            "1963",
            "1776",
            "1648",
    };

    private static String[] countryHistory = {
            "Pada tanggal inilah yang menjadi hari paling bersejarah bagi Bangsa Indonesia. Kemerdekaan Indonesia ditandai dengan pembacaan teks proklamasi oleh Soekarno pada tanggal 17 Agustus 1945. Dihadiri oleh para tokoh pergerakan kemerdekaan dan seluruh rakyat Indonesia yang ingin menyaksikan buah hasil dari perjuangan para pahlawan dan tokoh penting demi kemerdekaan Indonesia. Upacara pembacaan teks proklamasi tersebut berjalan dengan sangat lancar dengan bertempat di kediaman Soekarno di jalan Pegangsaan Timur Nomer 56. Beberapa acara telah disusun dalam hari kemerdekaan Indonesia, seperti pengibaran bendera Merah Putih, dan beberapa sambutan oleh walikota pada saat itu yaitu Suwiryo dan dr. Muwardi.",

            "Nama Argentina diambil dari istilah Latin argentum yang berarti 'perak'. Saat penjajah Spanyol mulai berlayar ke Río de la Plata kapal mereka karam, dan pemimpin ekspedisi Juan Díaz de Solís yang selamat diberi hadiah perak oleh para orang pribumi. Berita tentang legenda Sierra del Plata gunung perak sampai ke Spanyol sekitar tahun 1524. Orang Spanyol pun mulai menamakan sungai Solís, Río de la Plata (Sungai Perak). Nama Argentina sendiri pertama kali digunakan dalam buku Sejarah Penemuan, Populasi dan Penaklukan Río de la Plata (Historia del descubrimiento, población, y conquista del Río de la Plata) oleh Ruy Díaz de Guzmán's pada tahun 1612. dan menamakan daerah tersebut sebagai daerah Tierra Argentina (Tanah Perak).",

            "Revolusi Belgia pada tahun 1830 menimbulkan pendirian Belgia merdeka, Katolik, dan netral di bawah pemerintahan sementara dan kongres nasional. Sejak pelantikan Leopold I sebagai raja pada 1831, Belgia telah menjadi monarki konstitusional dan demokrasi parlementer. Antara kemerdekaan dan Perang Dunia II, sistem demokrasi berubah dari oligarki yang dicirikan oleh kedua pihak utama, Katolik dan liberal, menjadi sistem hak pilih universal yang termasuk pihak ke-3, Partai Buruh, dan peran kuat buat persatuan dagang. Aslinya, Bahasa Perancis, yang merupakan bahasa teradopsi dari kaum bangsawan dan borjuis, menjadi bahasa resmi. Sejak itu negeri itu mengembangkan sistem dwibahasa Bahasa Belanda – Bahasa Perancis.",

            "Kanada telah dihuni penduduk asli (dikenal di Kanada sebagai Bangsa Kanada) selama lebih dari 40.000 tahun. Ekspedisi bangsa Skandinavia mengunjunginya sekitar 1000, secara singkat tinggal di tempat yang dikenal sebagai L'Anse aux Meadows.",

            "Sejak awal periode Kolonial, selalu ada pemberontakan melawan penjajahan Spanyol. Kebanyakan gaampang dihancurkan atau terlalu lemah untuk mengubah keadaan. Pemberontakan terakhir yang berhasil mendapatkan kemerdekaan dari Spanyol, terjadi pada tahun 1810, mengikuti kemerdekaan St. Domingue di 1804 (dikenal sebagai Haiti), yang memberikan dukungan tak terbatas kepada pemimpin pemberontakan ini: Simón Bolívar dan Francisco de Paula Santander. Simón Bolívar akhirnya menjadi presiden pertama di Kolombia, dan Francisco de Paula Santander menjadi Wakil Presiden; ketika Simón Bolívar turun jabatan, Santander menjadi presiden kedua Kolombia. Pemberontakan akhirnya sukses pada tahun 1819 ketika teritori Sub Kerajaan Granada Baru menjadi Republik Kolombia Raya dengar federasi meliputi Ekuador dan Venezuela (Panama).",

            "Ibu kota kemudian disepakati pindah ke Berlin lagi pada tahun 1994 (berdasarkan Akta Berlin/Bonn). Relokasi pemerintahan baru selesai pada tahun 1999. Bonn sendiri mendapatkan status sebagai Bundesstadt (kota federal) karena menjadi tempat beberapa kementrian. Sejak bersatu, Jerman menjadi lebih aktif dalam keanggotaannya di Uni Eropa dan NATO. Jerman mengirim pasukan perdamaian untuk menjaga stabilitas di Balkan dan mengirim pasukan tentara ke Afganistan sebagai usaha meredam pasukan Taliban. Penurunan pasukan ini menjadi kontroversial karena menurut aturan domestik mereka, Jerman mengirim pasukan hanya untuk peran pertahanan.[34] Pada tahun 2005, Angela Merkel menjadi Kanselir Jerman wanita pertama dengan koalisi besar di pemerintahan.",

            "Jepang dimulai pada tahun 1603. Pada saat itu, Ieyasu yang telah berhasil menyatukan seluruh Jepang, membangun kekaisarannya di Edo, sekarang dikenal dengan Tokyo. Ieyasu mencoba membangun setiap aspek di negara ini sehingga negara ini mampu berdiri sendiri tanpa bantuan dari negara lain. Hasil dari politik yang dilakukan Ieyasu ini kemudian dimanfaatkan oleh Kekaisaran Tokugawa pada tahun 1639 dengan lahirnya Politik Isolasi. Latar belakang dari lahirnya Politik Isolasi ini banyaknya misionaris Kristen yang datang menyebarkan Agama Kristen. Berkembangnya Agama Kristen akan menjadi mimpi buruk bagi kekaisaran, oleh sebab itu Kaisar mengambil langkah untuk tidak berhubungan dengan negara asing, kecuali dengan Pedagang-Pedagang Belanda yang dinilai menguntungkan. Itu pun hanya dilakukan di satu tempat, yaitu di Pulau Dejima, Nagasaki.",


            "Tunku Abdul Rahman kemudian melakukan pertemuan di London dengan pihak Inggris. Tujuannya ialah memerdekakan Malaysia dari penjajahan Inggris. Berkali-kali perundingan dilakukan dan akhirnya pada 8 Februari 1956, Inggris menyetujui/memberikan kemerdekaan kepada Federasi Malaysia. Harusnya tanggal 8 Februari 1956 itulah hari kemerdekaan Malaysia, namun karena ada berbagai hambatan maka proklamasi kemerdekaan Malaysia baru didengungkan pada 31 Agustus 1957. Tunku Abdul Rahman lantas menjadi Perdana Menteri pertama Malaysia. Tanpa perjuangan getir, tanpa peperangan mengusir penjajah, Malaysia boleh merdeka. Sempat menjadi kekuatan ekonomi dan militer terbaik di Asia Tenggara, kini di usia kemerdekaan ke-61 tahun, Malaysia diancam dengan kebangkrutan lantaran hutang negera sebanyak Rp 3.500 triliun akibat mega korupsi mantan PM-nya, Najib Razak.",

            "Dimulai pada tahun 1775, 13 koloni mulai merencanakan pembelotan terhadap pemerintahan Inggris dan memproklamasikan kemerdekaan mereka pada tahun 1776 sebagai United States of America.",

            "Di bawah pemerintahan Karel V (kaisar Romawi Suci, dan raja Spanyol) kawasan ini (kini Belanda) merupakan salah satu dari 17 daerah Belanda, yaitu daerah yang meliputi sebagian besar kawasan yang dikenal hari ini sebagai Belgia, Luxemburg, dan Utara Prancis. Selepas mendapat kemerdekaan dari Phillip II (anak lelaki Karel V) pada 1648, Belanda menjadi sebuah negara republik yang dinamakan Republik Tujuh Provinsi (Republiek der Zeven Provinciën). Republik ini menjadi penguasa ekonomi, dan penjelajah laut yang mahir pada abad ke 17. Zaman ini dikenal sebagai Zaman Keemasan Belanda. Antara perusahaan-perusahaan internasional yang berawal di sini termasuk VOC.",

    };

    private static int[] countryImages = {
            R.drawable.id, R.drawable.ar, R.drawable.be, R.drawable.ca, R.drawable.co, R.drawable.de, R.drawable.jp, R.drawable.my, R.drawable.us, R.drawable.nl
    };

    static ArrayList<Country> getListData(){
        ArrayList<Country> list = new ArrayList<>();
        for (int position =0; position < countryNames.length; position++){
            Country country = new Country();
            country.setName_country(countryNames[position]);
            country.setImgCountry(countryImages[position]);
            country.setFormed_year(formedYear[position]);
            country.setHistory(countryHistory[position]);
            list.add(country);
        }
        return list;
    }
}

