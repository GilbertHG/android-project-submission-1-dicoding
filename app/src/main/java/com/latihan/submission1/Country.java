package com.latihan.submission1;

public class Country {
    private String name_country;
    private String formed_year;
    private String history;

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    private int imgCountry;

    public String getName_country() {
        return name_country;
    }

    public void setName_country(String name_country) {
        this.name_country = name_country;
    }

    public String getFormed_year() {
        return formed_year;
    }

    public void setFormed_year(String formed_year) {
        this.formed_year = formed_year;
    }

    public int getImgCountry() {
        return imgCountry;
    }

    public void setImgCountry(int imgCountry) {
        this.imgCountry = imgCountry;
    }
}
