package com.latihan.submission1;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class ListCountryAdapter extends RecyclerView.Adapter<ListCountryAdapter.ListViewHolder> {
    private ArrayList<Country> listCountry;
    private Context context;

    public ListCountryAdapter(ArrayList<Country> listCountry) {
        this.listCountry = listCountry;
    }

    public ListCountryAdapter(ArrayList<Country> listCountry, Context context) {
        this.listCountry = listCountry;
        this.context = context;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_country, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        Country country = listCountry.get(position);
        Glide.with(holder.itemView.getContext())
                .load(country.getImgCountry())
                .apply(new RequestOptions().override(55, 55))
                .into(holder.imgCountry);
        holder.tvCountryName.setText(country.getName_country());;
    }

    @Override
    public int getItemCount() {
        return listCountry.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        ImageView imgCountry;
        TextView tvCountryName, tvFormedYear, tvHistory;
        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            imgCountry = itemView.findViewById(R.id.img_country_flag);
            tvCountryName = itemView.findViewById(R.id.tv_country_name);
            tvFormedYear = itemView.findViewById(R.id.formed_year);
            tvHistory = itemView.findViewById(R.id.history);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    Country country = listCountry.get(position);
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtra(DetailActivity.EXTRA_COUNTRY, country.getName_country());
                    intent.putExtra(DetailActivity.EXTRA_IMG, country.getImgCountry());
////                    intent.putExtra(DetailActivity.EXTRA_FORMED_YEAR, country.getFormed_year());
////                    intent.putExtra(DetailActivity.EXTRA_HISTORY, country.getHistory());
                    intent.putExtra(DetailActivity.EXTRA_INDEX, position);
                    context.startActivity(intent);
                }
            });
        }
    }
}
