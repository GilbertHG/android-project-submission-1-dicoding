package com.latihan.submission1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rvCountry;
//    private String[] dataImg;
//    private String[] dataCountryName;
//    private String[] dataFormedYear;
//    private String[] dataHistory;
    private ArrayList<Country> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvCountry = findViewById(R.id.rv_country);
        rvCountry.setHasFixedSize(true);

        list.addAll(CountryData.getListData());
        showRecyclerList();

    }
    private void showRecyclerList(){
        rvCountry.setLayoutManager(new LinearLayoutManager(this));
        ListCountryAdapter listCountryAdapter = new ListCountryAdapter(list, this);
        rvCountry.setAdapter(listCountryAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.about, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.about:
            startActivity(new Intent(MainActivity.this, AboutActivity.class));
            return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

}
